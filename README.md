# EurKey DH-Mod ISO ES

Android - Multiling O Keyboard pie.1.0.2

![Android](/Android/Capturas/EurKey_ISO.jpg)

Windows - Teclado en pantalla

![Windows](/Windows/EurKey_DHm_ISO_es_odk.jpg)

QMK Configurator - XD75RE Ortholinear 5x15

![XD75RE](/XD75RE/Capturas/XD75RE_Layout4_Layer0.png)

ErgoDox EZ - Hardware

![ErgoDox EZ](/Ergodox/EurKey by Hardware.png)

TypeFu

![TypeFu](/TypeFu/EurKey_TypeFu.png)

Klavaro

![Klavaro](/Klavaro/Eurkey_Klavaro.png)


# Enlaces

- Colemak layout - Shai Coleman: https://colemak.com/
- Original EurKey layout - Steffen Brüntjen: https://eurkey.steffen.bruentjen.eu/
- Colemak DH Mod: https://colemakmods.github.io/mod-dh/
- EurKey DH Mod adaptation - Ganz Marcel: https://gitlab.com/jungganz/eurkey-colemak-mod-dh
- Multiling O Keyboard - Honso: https://play.google.com/store/apps/details?id=kl.ime.oh
- Microsoft Keyboard Layout Creator: https://www.microsoft.com/en-us/download/details.aspx?id=102134
- QMK Configurator: https://config.qmk.fm/
- ErgoDox EZ: https://ergodox-ez.com/
- Type Fu: https://type-fu.com/app
- Palabras del idioma Español: https://github.com/JorgeDuenasLerin/diccionario-espanol-txt
